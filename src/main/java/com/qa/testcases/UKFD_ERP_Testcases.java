package com.qa.testcases;
import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.qa.excelReader.ExcelReader;
import com.qa.excelReader.ExcelReaderByMap;
import com.qa.excelReader.ExcelWriterByColumnName;
import com.qa.pages.CashSale;
import com.qa.pages.CreditMemoPage;
import com.qa.pages.CustomerPage;
import com.qa.pages.CustomerRefundPage;
import com.qa.pages.ItemFulfilment;
import com.qa.pages.ItemReceiptPage;
import com.qa.pages.LoginPage;
import com.qa.pages.OpportunityPage;
import com.qa.pages.PurchaseOrderPage;
import com.qa.pages.QuotePage;
import com.qa.pages.ReturnAuthorizationPage;
import com.qa.pages.SalesOrderPage;
import com.qa.pages.SearchesforTransferOrder;
import com.qa.pages.TransferOrder;
import com.qa.pages.TransferOrderToCreateSearch;
import com.qa.util.TestUtil;
import com.sun.mail.imap.protocol.Item;

import net.bytebuddy.build.Plugin.Factory.UsingReflection.Priority;

public class UKFD_ERP_Testcases extends TestUtil {

	
	String path="C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx";
	ExtentTest test;
	ExtentReports extent;
	ExtentSparkReporter htmlReporter;
	ExcelReader reader;
	LoginPage loginPage;
	CustomerPage customerPage;
	OpportunityPage opprPage;
	QuotePage quotePage;
	ItemFulfilment itemfulfilmentPage;
	ReturnAuthorizationPage returnPage;
	ItemReceiptPage itemReceiptPage;
	CreditMemoPage creditMemoPage;
	CustomerRefundPage customerRefundPage;
	CashSale cashSale;
	PurchaseOrderPage poPage;
	SalesOrderPage soPage;
	TestUtil testBase;
	TransferOrder transferOrder;
	SearchesforTransferOrder searches;
	TransferOrderToCreateSearch transferOrdertoCreate;
	public void send_email() throws EmailException {
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath("./GrowthProjectReport/GrowthProjectReport.html");
		attachment.setDisposition(EmailAttachment.ATTACHMENT);
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("sindhuja.b@tvarana.com", "Sindhu@123"));
		email.setSSLOnConnect(true);
		email.addTo("sindhuja.b@tvarana.com", "Sindhuja");
		email.setFrom("sindhuja.b@tvarana.com", "Sindhuja");
		email.setSubject("Growth Project Test Report");
		email.setMsg("Here is the report please find the attachment");
		email.attach(attachment);
		email.send();
	}

	@BeforeTest
	public void setExtent() {
		// specify location of the report
		htmlReporter = new ExtentSparkReporter(
				System.getProperty("user.dir") + "/GrowthProjectReport/GrowthProjectReport.html");
		htmlReporter.config().setDocumentTitle("Growth Project Test Report"); // Tile of report
		htmlReporter.config().setReportName("Growth Project Test Report"); // Name of the report
		htmlReporter.config().setTheme(Theme.STANDARD);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// Passing General information
		extent.setSystemInfo("Environemnt", "QA");
		extent.setSystemInfo("user", "Sindhuja");
	}

	@AfterTest
	public void endReport() throws EmailException {
		extent.flush();
		//send_email();
	}


	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
		} else if (result.getStatus() == ITestResult.SKIP) {
			extent.removeTest(test);

		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}
		// driver.quit();
	}

	
	
	@BeforeClass
	public void setUp() throws InterruptedException {
		testBase=new TestUtil();
		testBase.setUp();
	}
	
	
	
	
	
	
	//running all tc's for multisite
	
//	@Test(priority = 2)
//	public void NewSalesOrderCreationforSampleItemsTrade() throws InvalidFormatException, IOException
//	{
//		ExtentTest test1 = null;
//		test = extent.createTest("Verifying Sales order Creation using sample items for trade customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","Sample_Trade".trim());
//		for (Map<String,String> data: testData) {
//			String CustomerFirstname = data.get("CustomerName");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String bin=data.get("Bin");
//			String id=data.get("CustomerId");
//			String lastfourdigits=data.get("lastfourdigits");
//			String terms="";
//			try
//			{
//				
//				
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//				customerPage=new CustomerPage();
//				soPage=new SalesOrderPage();
//				itemfulfilmentPage=new ItemFulfilment();
//				itemReceiptPage =new ItemReceiptPage();
//				poPage=new PurchaseOrderPage();
//				test1= test.createNode("Verifying sales order creation for item/items "+itemname);
//				customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
//				customerPage.clickNewSOFromCustomer();
//				soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,lastfourdigits, test1);
//				 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//				String so_url=driver.getCurrentUrl();
//				soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//				soPage.verifyEmail("Thanks for your sample order!", test1);
//				reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//				reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying Sales order Creation using sample items for trade customer");
//
//
//			}
//				
//			catch(Exception e)
//			{
//				test.fail("New Sales order creation for sample items is failed due to "+e.fillInStackTrace());
//			}
//
//		}
//		
//	}
//	
	
//	@Test(priority = 2)
//	public void NewSalesOrderCreationforSampleItems() throws InvalidFormatException, IOException
//	{
//		ExtentTest test1 = null;
//		test = extent.createTest("Verifying Sales order Creation using sample items for general customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","Sample_general".trim());
//		for (Map<String,String> data: testData) {
//			String CustomerFirstname = data.get("CustomerName");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String bin=data.get("Bin");
//			String id=data.get("CustomerId");
//			String lastfourdigits=data.get("lastfourdigits");
//			String terms="";
//			try
//			{
//				
//				
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//				customerPage=new CustomerPage();
//				soPage=new SalesOrderPage();
//				itemfulfilmentPage=new ItemFulfilment();
//				itemReceiptPage =new ItemReceiptPage();
//				poPage=new PurchaseOrderPage();
//				test1= test.createNode("Verifying sales order creation for item/items "+itemname);
//				customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
//				customerPage.clickNewSOFromCustomer();
//				soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,lastfourdigits, test1);
//				 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//				String so_url=driver.getCurrentUrl();
//				soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//				soPage.verifyEmail("Thanks for your sample order!", test1);
//				reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//				reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying Sales order Creation using sample items for general customer");
//
//
//			}
//				
//			catch(Exception e)
//			{
//				test.fail("New Sales order creation for sample items is failed due to "+e.fillInStackTrace());
//			}
//
//		}
//		
//	}
//	

//	
//	@Test(priority = 1)
//	public void PackedItemsGeneral() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying Sales Order Creation using Packed Items for general customer ");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","Packed_itemsGeneral".trim());
//		for (Map<String,String> data: testData) {
//			String CustomerFirstname = data.get("CustomerName");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String bin=data.get("Bin");
//			String id=data.get("CustomerId");
//			String lastfourdigits=data.get("lastfourdigits");
//			String terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying new sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying Sales Order Creation using Packed Items for general customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//					String sales_order_url=driver.getCurrentUrl();
//					
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
//	@Test(priority = 1)
//	public void PackedItemsGeneralTrade() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying Sales Order Creation using Packed Items for trade customer ");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","PackedTrade".trim());
//		for (Map<String,String> data: testData) {
//			String CustomerFirstname = data.get("CustomerName");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String bin=data.get("Bin");
//			String id=data.get("CustomerId");
//			String lastfourdigits=data.get("lastfourdigits");
//			String terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying new sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying Sales Order Creation using Packed Items for trade customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//					String sales_order_url=driver.getCurrentUrl();
//					
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}


//	@Test(priority = 2)
//	public void PackedItemsGeneralwithMultipleLocations() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying Sales Order Creation for Packed Items with multiple locations for general customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","Packed_items_multiple_locGenerl".trim());
//		for (Map<String,String> data: testData) {
//			String CustomerFirstname = data.get("CustomerName");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String bin=data.get("Bin");
//			String id=data.get("CustomerId");
//			String lastfourdigits=data.get("lastfourdigits");
//			String terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying new sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying Sales Order Creation for Packed Items with multiple locations for general customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifyProcessedScreen(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//					String sales_order_url=driver.getCurrentUrl();
//					
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
//	@Test(priority = 2)
//	public void PackedItemsGeneralwithMultipleLocationsTrade() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying Sales Order Creation for Packed Items with multiple locations for trade customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","PackedMultipleTrade".trim());
//		for (Map<String,String> data: testData) {
//			String CustomerFirstname = data.get("CustomerName");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String bin=data.get("Bin");
//			String id=data.get("CustomerId");
//			String lastfourdigits=data.get("lastfourdigits");
//			String terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying new sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying Sales Order Creation for Packed Items with multiple locations for trade customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifyProcessedScreen(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//					String sales_order_url=driver.getCurrentUrl();
//					
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
//	@Test(priority = 1)
//	public void Acc_Packed_Rolled_Items() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying sales order creation for Accessory , Packed , Rolled Items for general customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","Rolled&Packed&AccGenrl".trim());
//		for (Map<String,String> data: testData) {
//			String Customername = data.get("Customer_Name");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String returnquantity=data.get("ReturnQuantity");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String securitycode=data.get("Security_Code");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String Supplier_Delivery_Note=data.get("SupplierDeliveryNote");
//			String Bin=data.get("Bin");
//			String lastfourdigits=data.get("lastfourdigits");
//			String customerid=data.get("CustomerId");
//			String Terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,customerid);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, Customername, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "GeneralLinks");
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "soLink", so_url);
//					reader2.setCellData(path, "GeneralLinks", rowNumber, "TestCase", "Verifying sales order creation for Accessory , Packed , Rolled Items for general customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifyProcessedScreen(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"GeneralLinks");
//					soPage.verifyCashSaleandPO("Purchase Order", test1); 
//					String sales_order_url=driver.getCurrentUrl();
//					soPage.navigatetoTransaction("Purchase Order");
//					poPage.navigateToReceiveFromPO("Fulfilment");
//					itemReceiptPage.saveItemReceiptfromPO(Supplier_Delivery_Note, test1);
//					driver.navigate().to(sales_order_url);
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("  Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
//	@Test(priority = 1)
//	public void FullOrderReturnGeneral() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying full order return for general customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","FullReturnRetail".trim());
//		for (Map<String,String> data: testData) {
//			String Customername = data.get("Customer_Name");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String returnquantity=data.get("ReturnQuantity");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String securitycode=data.get("Security_Code");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String Supplier_Delivery_Note=data.get("SupplierDeliveryNote");
//			String Bin=data.get("Bin");
//			String lastfourdigits=data.get("lastfourdigits");
//			String customerid=data.get("CustomerId");
//			String Terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,customerid);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, Customername, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "Returns");
//					reader2.setCellData(path, "Returns", rowNumber, "soLink", so_url);
//					 reader2.setCellData(path, "Returns", rowNumber, "itemname", itemname);
//					reader2.setCellData(path, "Returns", rowNumber, "TestCase", "Verifying full order return for general customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifyProcessedScreen(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"Returns");
//					soPage.verifyCashSaleandPO("Purchase Order", test1); 
//					String sales_order_url=driver.getCurrentUrl();
//					soPage.navigatetoTransaction("Purchase Order");
//					poPage.navigateToReceiveFromPO("Fulfilment");
//					itemReceiptPage.saveItemReceiptfromPO(Supplier_Delivery_Note, test1);
//					driver.navigate().to(sales_order_url);
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("  Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
//	@Test(priority = 2)
//	public void FullOrderReturnTrade() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying full order return for trade customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","TradeFull".trim());
//		for (Map<String,String> data: testData) {
//			String Customername = data.get("Customer_Name");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String returnquantity=data.get("ReturnQuantity");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String securitycode=data.get("Security_Code");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String Supplier_Delivery_Note=data.get("SupplierDeliveryNote");
//			String Bin=data.get("Bin");
//			String lastfourdigits=data.get("lastfourdigits");
//			String customerid=data.get("CustomerId");
//			String Terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,customerid);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, Customername, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "Returns");
//					reader2.setCellData(path, "Returns", rowNumber, "soLink", so_url);
//					 reader2.setCellData(path, "Returns", rowNumber, "itemname", itemname);
//					reader2.setCellData(path, "Returns", rowNumber, "TestCase", "Verifying full order return for trade customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifyProcessedScreen(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"Returns");
//					soPage.verifyCashSaleandPO("Purchase Order", test1); 
//					String sales_order_url=driver.getCurrentUrl();
//					soPage.navigatetoTransaction("Purchase Order");
//					poPage.navigateToReceiveFromPO("Fulfilment");
//					itemReceiptPage.saveItemReceiptfromPO(Supplier_Delivery_Note, test1);
//					driver.navigate().to(sales_order_url);
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("  Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
	@Test(priority = 3)
	public void PartailOrderReturnTrade() throws Exception 
	{
		
		ExtentTest test1 = null;
		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
		test=extent.createTest("Verifying transfer order for trade customer");
		ExcelReaderByMap reader2 = new ExcelReaderByMap();
		int row=1;
		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","RollTest".trim());
		for (Map<String,String> data: testData) {
			String Customername = data.get("Customer_Name");
			String itemname=data.get("Item_Name");
			String quantity=data.get("Quantity");
			String returnquantity=data.get("ReturnQuantity");
			String shippingmethod=data.get("Shipping_Method");
			String delivery_instructions=data.get("Delivery_Instructions");
			String payment_method=data.get("Payment_Method");
			String securitycode=data.get("Security_Code");
			String nameoncard=data.get("NameOnCard");
			String form=data.get("Sales_Order_Form");
			String Supplier_Delivery_Note=data.get("SupplierDeliveryNote");
			String Bin=data.get("Bin");
			String lastfourdigits=data.get("lastfourdigits");
			String customerid=data.get("CustomerId");
			String TransferItem=data.get("TransferItem");
			String Terms="";
//			try
//			{
				String Customer_url="https://3460739-sb2.app.netsuite.com/app/common/entity/custjob.nl?id=";

				 test1= test.createNode("Verifying sales order creation for item/items "+itemname+" using "+payment_method);
					loginPage=new LoginPage();
					customerPage=new CustomerPage();
					soPage=new SalesOrderPage();
					itemfulfilmentPage=new ItemFulfilment();
					returnPage=new ReturnAuthorizationPage();
					itemReceiptPage=new ItemReceiptPage(); 
					creditMemoPage=new CreditMemoPage();
					customerRefundPage=new CustomerRefundPage();
					cashSale=new CashSale();
					transferOrder=new TransferOrder();
					searches=new SearchesforTransferOrder();
					poPage=new PurchaseOrderPage();
					transferOrdertoCreate=new TransferOrderToCreateSearch();
					customerPage.navigateToCustomer("Distribution",Customer_url,customerid);
					customerPage.clickNewSOFromCustomer();
					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, Customername, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
					soPage.verifyEmail("Thanks for your order!", test1);
					String so_url=driver.getCurrentUrl();
					 int rowNumber = reader2.getRowNumber(path, "Returns");
					 reader2.setCellData(path, "Returns", rowNumber, "itemname", itemname);
					reader2.setCellData(path, "Returns", rowNumber, "soLink", so_url);
					reader2.setCellData(path, "Returns", rowNumber, "TestCase", "Verifying transfer order return for trade customer");
					soPage.salesOrderApproval(test1);
					soPage.verifyProcessedScreen(test1);
					String tranNo=soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"Returns");
				soPage.verifyCashSaleandPO("Purchase Order", test1); 
					String sales_order_url=driver.getCurrentUrl();
					soPage.navigatetoTransaction("Purchase Order");
					poPage.navigateToReceiveFromPO("Distribution");
					itemReceiptPage.saveItemReceiptfromPO(Supplier_Delivery_Note, test1);
					driver.navigate().to(sales_order_url);
					transferOrdertoCreate.navigate_to_search(tranNo, test1);
					String toId=transferOrder.create_Transfer_Order(TransferItem.trim(),"2",test1);
					String url=driver.getCurrentUrl();
					searches.verifyTOtoShipFromHincleySearch(toId, test1);
					driver.navigate().to(url);
					itemfulfilmentPage.fulfillOrder(TransferItem, "2", Bin, test1);
					searches.verifyAfterTOtoShipFromHincleySearch(toId, test1);
					searches.verifyTOPendingReceiptSearch(toId, test1);
					driver.navigate().to(url);
					poPage.navigateToReceive();
					itemReceiptPage.saveItemReceiptfromTO("test", test1);
					searches.verifyAfterPendingReceiptSearch(toId, test1);

					
					
					
					
							
			//}
//			catch(Exception e)
//			{
//				test1.fail("  Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
		
		
	}
	}
//}
//	@Test(priority = 4)
//	public void PartailOrderReturnRetail() throws Exception 
//	{
//		
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		test=extent.createTest("Verifying partial order return for general customer");
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","PartialReturnRetail".trim());
//		for (Map<String,String> data: testData) {
//			String Customername = data.get("Customer_Name");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String returnquantity=data.get("ReturnQuantity");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String securitycode=data.get("Security_Code");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String Supplier_Delivery_Note=data.get("SupplierDeliveryNote");
//			String Bin=data.get("Bin");
//			String lastfourdigits=data.get("lastfourdigits");
//			String customerid=data.get("CustomerId");
//			String Terms="";
//			try
//			{
//				String Customer_url="https://3460739-rp.app.netsuite.com/app/common/entity/custjob.nl?id=";
//
//				 test1= test.createNode("Verifying sales order creation for item/items "+itemname+" using "+payment_method);
//					loginPage=new LoginPage();
//					customerPage=new CustomerPage();
//					soPage=new SalesOrderPage();
//					itemfulfilmentPage=new ItemFulfilment();
//					returnPage=new ReturnAuthorizationPage();
//					itemReceiptPage=new ItemReceiptPage(); 
//					creditMemoPage=new CreditMemoPage();
//					customerRefundPage=new CustomerRefundPage();
//					cashSale=new CashSale();
//					poPage=new PurchaseOrderPage();
//					customerPage.navigateToCustomer("Sales Manager",Customer_url,customerid);
//					customerPage.clickNewSOFromCustomer();
//					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, Customername, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
//					soPage.verifyEmail("Thanks for your order!", test1);
//					String so_url=driver.getCurrentUrl();
//					 int rowNumber = reader2.getRowNumber(path, "Returns");
//					 reader2.setCellData(path, "Returns", rowNumber, "itemname", itemname);
//					reader2.setCellData(path, "Returns", rowNumber, "soLink", so_url);
//					reader2.setCellData(path, "Returns", rowNumber, "TestCase", "Verifying partial order return for general customer");
//					soPage.salesOrderApproval(test1);
//					soPage.verifyProcessedScreen(test1);
//					soPage.verifySOStatus("PENDING FULFILLMENT", test1,rowNumber,"Returns");
//					soPage.verifyCashSaleandPO("Purchase Order", test1); 
//					String sales_order_url=driver.getCurrentUrl();
//					soPage.navigatetoTransaction("Purchase Order");
//					poPage.navigateToReceiveFromPO("Fulfilment");
//					itemReceiptPage.saveItemReceiptfromPO(Supplier_Delivery_Note, test1);
//					driver.navigate().to(sales_order_url);
//							
//			}
//			catch(Exception e)
//			{
//				test1.fail("  Sales order creation for item/items "+itemname+" using "+payment_method+" is failed due to "+e.fillInStackTrace());
//			}
//		
//		
//	}
//	}
//}
//	@Test()
//	public void FulfillmentOfAllTheOrders() throws Exception
//	{
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		List<String> list=new ArrayList<String>();  
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\ReleasePreview.xlsx","Test".trim());
//		for (Map<String,String> data: testData) {
//			String testcase=data.get("TestCase");
//			String quantity=data.get("Quantity");
//			String item=data.get("itemname");
//			String bin=data.get("Bin");
//			//String status=data.get("Status");
//			String solink=data.get("soLink");
//			String paymentMethod=data.get("paymentMethod");
//			String returnquantity=data.get("ReturnQuantity");
//			String soNumber=data.get("soNumber");
//			String returnreason=data.get("Return_Reason");
//			String returnresponsibility=data.get("Return_Responsibility");
//			String returnnotes=data.get("Return_Notes");
//			String returnsubtype=data.get("Return_Subtype");
//			String Return_Reason3=data.get("Return_Reason3");
//			String Return_reason4=data.get("Return_reason4");
//			String Supplier_Delivery_Note=data.get("Supplier_Delivery_Note");
//			list.add(testcase);
//			poPage=new PurchaseOrderPage();
//			itemfulfilmentPage=new ItemFulfilment();
//			soPage=new SalesOrderPage();
//			loginPage=new LoginPage();
//			 customerPage = new CustomerPage();
//			soPage=new SalesOrderPage();
//			itemfulfilmentPage=new ItemFulfilment();
//			returnPage=new ReturnAuthorizationPage();
//			itemReceiptPage=new ItemReceiptPage(); 
//			creditMemoPage=new CreditMemoPage();
//			customerRefundPage=new CustomerRefundPage();
//			cashSale=new CashSale();
//			poPage=new PurchaseOrderPage();
//	        if(Collections.frequency(list, testcase)>1)
//	       	{
//	        		test1=test.createNode("verifying post Return for "+item);
//	       	}
//	        else
//	       	{
//	    		test=extent.createTest(testcase);
//        		test1=test.createNode("verifying post Return for "+item);
//	       	}
//			Map<String, String> paymentData=null;
//
//	    	driver.navigate().to(solink);
//	    	 if(!(paymentMethod.equals("Bank Transfer") || paymentMethod.contains("PayPal (UKFD Site)")))
//			 {
//			 
//				 paymentData = soPage.getPaymentData();
//			 }
//		soPage.waitUntilStockIsAutoCommitted(quantity, test1,item);
//		itemfulfilmentPage.fulfillOrder(item, quantity, bin, test1);
//		itemfulfilmentPage.navigateToSoFromFulfillment();
//		soPage.verifySOStatusFul("BILLED",test1);		
//		soPage.navigate_to_return_authorization(test1);
//		returnPage.saveReturnOrder(quantity,returnquantity,returnreason, returnresponsibility, returnnotes, returnsubtype, Return_Reason3, Return_reason4,Supplier_Delivery_Note, test1);
//		returnPage.navigateToItemReceipt("Fulfilment");
//		itemReceiptPage.saveItemReceipt(Supplier_Delivery_Note,test1);
//		itemReceiptPage.verifyItemReceiptStatus(test1);
//		itemReceiptPage.navigateToReturnAuthorization("Customer Service");
//		itemReceiptPage.navigateToCreditMemo("Finance");
//		creditMemoPage.saveCreditMemo();
//		creditMemoPage.verifyCreditMemoStatus(test1);
//		creditMemoPage.navigateToCustomerRefund();
//		customerRefundPage.saveCustomerRefund(paymentData, test1,paymentMethod);
//	        
//			
//		}
//	}
//	
}


	
	
	