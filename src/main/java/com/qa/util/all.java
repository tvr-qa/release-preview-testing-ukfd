package com.qa.util;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.qa.excelReader.ExcelReader;
import com.qa.excelReader.ExcelReaderByMap;
import com.qa.excelReader.ExcelWriterByColumnName;
import com.qa.pages.CashSale;
import com.qa.pages.CreditMemoPage;
import com.qa.pages.CustomerPage;
import com.qa.pages.CustomerRefundPage;
import com.qa.pages.ItemFulfilment;
import com.qa.pages.ItemReceiptPage;
import com.qa.pages.LoginPage;
import com.qa.pages.OpportunityPage;
import com.qa.pages.PurchaseOrderPage;
import com.qa.pages.QuotePage;
import com.qa.pages.ReturnAuthorizationPage;
import com.qa.pages.SalesOrderPage;
import com.qa.util.TestUtil;
import com.sun.mail.imap.protocol.Item;

public class all extends TestUtil {

	
	String path="C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx";
	ExtentTest test;
	ExtentReports extent;
	ExtentSparkReporter htmlReporter;
	ExcelReader reader;
	LoginPage loginPage;
	CustomerPage customerPage;
	OpportunityPage opprPage;
	QuotePage quotePage;
	ItemFulfilment itemfulfilmentPage;
	ReturnAuthorizationPage returnPage;
	ItemReceiptPage itemReceiptPage;
	CreditMemoPage creditMemoPage;
	CustomerRefundPage customerRefundPage;
	CashSale cashSale;
	PurchaseOrderPage poPage;
	SalesOrderPage soPage;
	TestUtil testBase;
	public void send_email() throws EmailException {
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath("./GrowthProjectReport/GrowthProjectReport.html");
		attachment.setDisposition(EmailAttachment.ATTACHMENT);
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("sindhuja.b@tvarana.com", "Sindhu@123"));
		email.setSSLOnConnect(true);
		email.addTo("sindhuja.b@tvarana.com", "Sindhuja");
		email.setFrom("sindhuja.b@tvarana.com", "Sindhuja");
		email.setSubject("Growth Project Test Report");
		email.setMsg("Here is the report please find the attachment");
		email.attach(attachment);
		email.send();
	}

	@BeforeTest
	public void setExtent() {
		// specify location of the report
		htmlReporter = new ExtentSparkReporter(
				System.getProperty("user.dir") + "/GrowthProjectReport/GrowthProjectReport.html");
		htmlReporter.config().setDocumentTitle("Growth Project Test Report"); // Tile of report
		htmlReporter.config().setReportName("Growth Project Test Report"); // Name of the report
		htmlReporter.config().setTheme(Theme.STANDARD);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// Passing General information
		extent.setSystemInfo("Environemnt", "QA");
		extent.setSystemInfo("user", "Sindhuja");
	}

	@AfterTest
	public void endReport() throws EmailException {
		extent.flush();
		send_email();
	}


	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
		} else if (result.getStatus() == ITestResult.SKIP) {
			extent.removeTest(test);

		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}
		// driver.quit();
	}

	@DataProvider
	public Object[][] UKFD_RetailSalesOrder() throws IOException {
		reader = new ExcelReader();
		return reader.readExcelData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx", 0);
	}
	
	
	@BeforeClass
	public void setUp() throws InterruptedException {
		testBase=new TestUtil();
		testBase.setUp();
	}
	
	
//	
//	@Test()
//	public void FulfillmentOfAllTheOrders() throws Exception
//	{
//		ExtentTest test1 = null;
//		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		int row=1;
//		List<String> list=new ArrayList<String>();  
//
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","Fulfilment".trim());
//		
//		for (Map<String,String> data: testData) {
//			String testcase=data.get("Testcase");
//			String itemname=data.get("ItemName");
//			String quantity=data.get("Quantity");
//			String bin=data.get("Bin");
//			list.add(testcase);
//			System.out.println(list);
//			
//	        if(Collections.frequency(list, testcase)>1)
//	       	{
//	        		test1=test.createNode("verifying item fulfillmet for "+itemname);
//	       	}
//	        else
//	       	{
//	    		test=extent.createTest(testcase);
//        		test1=test.createNode("verifying item fulfillmet for "+itemname);
//	       	}
//	        
//			
//		}
//	}
//	
	
//	@Test()
//	public void UKFD_TradeSalesOrder() throws Exception 
//	{
//		ExtentTest test1=null;
//		test=extent.createTest("Verifying New Sales Order creation via Contact Centre / Showroom  - Credit/Debit Card ");
//		soPage=new SalesOrderPage();
//		customerPage=new CustomerPage();
//		opprPage=new OpportunityPage();
//		quotePage=new QuotePage();
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		String Terms="";
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","TestCase4".trim());
//		for (Map<String,String> data: testData) 
//		{
//			String CustomerFirstname = data.get("Customer_Firstname");
//			String CustomerLastname = data.get("Customer_Lastname");
//			String email = data.get("Email");
//			String addr1 = data.get("Address1");
//			String addr2 = data.get("Address2");
//			String addr3 = data.get("Address3"); 
//			String city = data.get("City");
//			String phone=data.get("Phone");
//			String state = data.get("State");
//			String zip = data.get("Zip");
//			String leadsource=data.get("Lead_source");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String creditcardno=data.get("Credit_Card_Number");
//			String expirydata=data.get("Expiry_Date");
//			String securitycode=data.get("Security_Code");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String customertype=data.get("Customer_type");
//			String bin=data.get("Bin");
//			String lastfourdigits=data.get("lastfourdigits");
//			try
//			{
//				
//				test1= test.createNode("Verifying sales order creation for item "+itemname+" using "+payment_method);
//				 customerPage = new CustomerPage();
//				 soPage=new SalesOrderPage();
//				 opprPage=new OpportunityPage();
//			 	 quotePage=new QuotePage();
//				 customerPage.enter_Customer_details(CustomerFirstname, CustomerLastname, email, phone, addr1, addr2, addr3, city, state, zip, customertype,"Trade Sales",creditcardno,nameoncard,expirydata,securitycode,payment_method, test1);
//				 opprPage.enterOpprntyDetails(leadsource,test1);
//				 quotePage.enterQuoteDetails(location,itemname,quantity,shippingmethod,test1);
//				 soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
//				 soPage.verifyEmail("Thanks for your order!", test1);
//				 String so_url=driver.getCurrentUrl();
//				 int rowNumber = reader2.getRowNumber(path, "Fulfilment");
//				reader2.setCellData(path, "Fulfilment", rowNumber, "soLink", so_url);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "Quantity", quantity);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "itemName", itemname);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "Bin", bin);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "Testcase", "Verifying New Sales Order creation via Contact Centre / Showroom  - Credit/Debit Card");
//				 soPage.salesOrderApproval(test1);
//				 soPage.verifySOStatus("PENDING FULFILLMENT",test1,rowNumber);
//				 soPage.verifyEmail("Your order has been confirmed", test1);
//				 soPage.verifyCashSaleandPO("Cash Sale", test1);
//			}
//		catch(Exception e)
//		{
//			test1.fail("Verifying Creation of new sales order with "+itemname +" using "+payment_method.trim() +" is failed due to " +e.fillInStackTrace());
//		}
//		
//		}
//			
//	}
//
//
//	//testcase 4
//	@Test()
//	public void UKFD_RetailSalesOrder1() throws Exception {
//	
//		ExtentTest test1=null;
//		test=extent.createTest("Verifying New Sales Order creation via Contact Centre / Showroom  - Credit/Debit Card ");
//		soPage=new SalesOrderPage();
//		customerPage=new CustomerPage();
//		opprPage=new OpportunityPage();
//		quotePage=new QuotePage();
//		ExcelReaderByMap reader2 = new ExcelReaderByMap();
//		String Terms="";
//		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","TestCase4".trim());
//		for (Map<String,String> data: testData) 
//		{
//			String CustomerFirstname = data.get("Customer_Firstname");
//			String CustomerLastname = data.get("Customer_Lastname");
//			String email = data.get("Email");
//			String addr1 = data.get("Address1");
//			String addr2 = data.get("Address2");
//			String addr3 = data.get("Address3"); 
//			String city = data.get("City");
//			String phone=data.get("Phone");
//			String state = data.get("State");
//			String zip = data.get("Zip");
//			String leadsource=data.get("Lead_source");
//			String itemname=data.get("Item_Name");
//			String quantity=data.get("Quantity");
//			String location=data.get("Location");
//			String shippingmethod=data.get("Shipping_Method");
//			String delivery_instructions=data.get("Delivery_Instructions");
//			String payment_method=data.get("Payment_Method");
//			String creditcardno=data.get("Credit_Card_Number");
//			String expirydata=data.get("Expiry_Date");
//			String securitycode=data.get("Security_Code");
//			String nameoncard=data.get("NameOnCard");
//			String form=data.get("Sales_Order_Form");
//			String customertype=data.get("Customer_type");
//			String bin=data.get("Bin");
//			String lastfourdigits=data.get("lastfourdigits");
//			try
//			{
//				
//				test1= test.createNode("Verifying sales order creation for item "+itemname+" using "+payment_method);
//				 customerPage = new CustomerPage();
//				 soPage=new SalesOrderPage();
//				 opprPage=new OpportunityPage();
//			 	 quotePage=new QuotePage();
//				 customerPage.enter_Customer_details(CustomerFirstname, CustomerLastname, email, phone, addr1, addr2, addr3, city, state, zip, customertype,"Trade Sales",creditcardno,nameoncard,expirydata,securitycode,payment_method, test1);
//				 opprPage.enterOpprntyDetails(leadsource,test1);
//				 quotePage.enterQuoteDetails(location,itemname,quantity,shippingmethod,test1);
//				 soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test1);
//				 soPage.verifyEmail("Thanks for your order!", test1);
//				 String so_url=driver.getCurrentUrl();
//				 int rowNumber = reader2.getRowNumber(path, "Fulfilment");
//				reader2.setCellData(path, "Fulfilment", rowNumber, "soLink", so_url);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "Quantity", quantity);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "itemName", itemname);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "Bin", bin);
//				reader2.setCellData(path, "Fulfilment", rowNumber, "Testcase", "Verifying New Sales Order creation via Contact Centre / Showroom  - Credit/Debit Card");
//				 soPage.salesOrderApproval(test1);
//				 soPage.verifySOStatus("PENDING FULFILLMENT",test1,rowNumber);
//				 soPage.verifyEmail("Your order has been confirmed", test1);
//				 soPage.verifyCashSaleandPO("Cash Sale", test1);
//			}
//		catch(Exception e)
//		{
//			test1.fail("Verifying Creation of new sales order with "+itemname +" using "+payment_method.trim() +" is failed due to " +e.fillInStackTrace());
//		}
//		
//		}
//			
//	}
//	
//	
////	@Test()
////	public void UKFD_PartialReturnPostShipment() throws Exception
////	{
////		ExtentTest test1 = null;
////		ExcelWriterByColumnName excelwrite=new ExcelWriterByColumnName();
////		test=extent.createTest("Verifying Partial Order Return (Post Shipmet)");
////		ExcelReaderByMap reader2 = new ExcelReaderByMap();
////		int row=1;
////		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","TestCase11".trim());
////		for (Map<String,String> data: testData) {
////			String Customername = data.get("Customer_Name");
////			String itemname=data.get("Item_Name");
////			String quantity=data.get("Quantity");
////			String returnquantity=data.get("ReturnQuantity");
////			String shippingmethod=data.get("Shipping_Method");
////			String delivery_instructions=data.get("Delivery_Instructions");
////			String payment_method=data.get("Payment_Method");
////			String securitycode=data.get("Security_Code");
////			String nameoncard=data.get("NameOnCard");
////			String form=data.get("Sales_Order_Form");
////			String returnreason=data.get("Return_Reason");
////			String returnresponsibility=data.get("Return_Responsibility");
////			String returnnotes=data.get("Return_Notes");
////			String returnsubtype=data.get("Return_Subtype");
////			String Return_Reason3=data.get("Return_Reason3");
////			String Return_reason4=data.get("Return_reason4");
////			String Supplier_Delivery_Note=data.get("Supplier_Delivery_Note");
////			String Bin=data.get("Bin");
////			String lastfourdigits=data.get("lastfourdigits");
////			try
////			{
////				    test1= test.createNode("Verifying Partial Order Return  for item/items "+itemname+" using "+payment_method);
////					loginPage=new LoginPage();
////					soPage=new SalesOrderPage();
////					itemfulfilmentPage=new ItemFulfilment();
////					returnPage=new ReturnAuthorizationPage();
////					itemReceiptPage=new ItemReceiptPage(); 
////					creditMemoPage=new CreditMemoPage();
////					customerRefundPage=new CustomerRefundPage();
////					cashSale=new CashSale();
////					Map<String, String> paymentData = null;
////					String Terms="yes";
////					loginPage.choose_required_role("Customer Service");
////					soPage.navigateToSO();
////					soPage.enterSoDetails(form, delivery_instructions, shippingmethod, Customername, itemname, quantity, payment_method.trim(),Terms,lastfourdigits, test);
////					String soUrl=driver.getCurrentUrl();
////					soPage.salesOrderApproval(test);
////					if(!payment_method.equals("Bank Transfer")&& !payment_method.equals("PayPal"))
////					{
////					  paymentData = new HashMap<>();
////				      paymentData = soPage.getPaymentData();
////					}
////					try
////					{
////						
////						
////					}
////					catch(Exception e)
////					{
////						
////					}
////					returnPage.saveReturnOrder(quantity,returnquantity,returnreason, returnresponsibility, returnnotes, returnsubtype, Return_Reason3, Return_reason4,Supplier_Delivery_Note, test);
////					returnPage.navigateToItemReceipt("Fulfilment");
////					itemReceiptPage.saveItemReceipt(Supplier_Delivery_Note,test);
////					itemReceiptPage.verifyItemReceiptStatus(test);
////					itemReceiptPage.navigateToReturnAuthorization("Customer Service");
////					itemReceiptPage.navigateToCreditMemo("Finance");
////					creditMemoPage.saveCreditMemo();
////					creditMemoPage.verifyCreditMemoStatus(test);
////					creditMemoPage.navigateToCustomerRefund();
////					customerRefundPage.saveCustomerRefund(paymentData, test);
//////					
////					String soLink = driver.getCurrentUrl();
////					int rowNumber = reader2.getRowNumber(path, "test");
////					reader2.setCellData(path, "Fulfilment", rowNumber, "SO Link", soLink);
////					reader2.setCellData(path, "Fulfilment", rowNumber, "Quantity", quantity);
////					reader2.setCellData(path, "Fulfilment", rowNumber, "Test Case Name", "New Sales order via Contact Centre / Showroom  - Bank Transfer");
////					
////			}
////			catch(Exception e)
////			{
////				
////				test1.fail("Partial Order Return  for item/items "+itemname+" using "+payment_method+"  "+e.fillInStackTrace());
////			
////			}
////			
////			
////		}
////			
////			
////		
////	}
//	
////	@Test(priority = 1)
////	public void TradeOrderCarpetVinyl() throws Exception 
////	{
////		ExtentTest test1 = null;
////		test = extent.createTest("Verifying Trade Order via Contact Centre - Carpet/Vinyl Credit Card");
////		ExcelReaderByMap reader2 = new ExcelReaderByMap();
////		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","TestCase34".trim());
////		for (Map<String,String> data: testData) {
////			String CustomerFirstname = data.get("Customer_Firstname");
////			String CustomerLastname = data.get("Customer_Lastname");
////			String email = data.get("Email");
////			String addr1 = data.get("Address1");
////			String addr2 = data.get("Address2");
////			String addr3 = data.get("Address3"); 
////			String city = data.get("City");
////			String phone=data.get("Phone");
////			String state = data.get("State");
////			String zip = data.get("Zip");
////			String leadsource=data.get("Lead_source");
////			String itemname=data.get("Item_Name");
////			String quantity=data.get("Quantity");
////			String location=data.get("Location");
////			String shippingmethod=data.get("Shipping_Method");
////			String delivery_instructions=data.get("Delivery_Instructions");
////			String payment_method=data.get("Payment_Method");
////			String creditcardno=data.get("Credit_Card_Number");
////			String expirydata=data.get("Expiry_Date");
////			String securitycode=data.get("Security_Code");
////			String nameoncard=data.get("NameOnCard");
////			String form=data.get("Sales_Order_Form");
////			String customertype=data.get("Customer_type");
////			String bin=data.get("Bin");
////			String note=data.get("SupplierDeliveryNote");
////			String Terms="";
////			String lastdigits=data.get("lastfourdigit");
////			try {
////				 test1= test.createNode("Verifying sales order creation for item "+itemname+" using "+payment_method);
////				 customerPage = new CustomerPage();
////				 soPage=new SalesOrderPage();
////				 opprPage=new OpportunityPage();
////			 	 quotePage=new QuotePage();
////			 	 poPage=new PurchaseOrderPage();
////			 	 itemReceiptPage=new ItemReceiptPage();
////				 customerPage.enter_Customer_details(CustomerFirstname, CustomerLastname, email, phone, addr1, addr2, addr3, city, state, zip, customertype,"Trade Sales",creditcardno,nameoncard,expirydata,securitycode,payment_method, test1);
////				 opprPage.enterOpprntyDetails(leadsource,test1);
////				 quotePage.enterQuoteDetails(location,itemname,quantity,shippingmethod,test1);
////				 soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),Terms,lastdigits, test1);
////				 soPage.verifyEmail("Thanks for your order!", test1);
////			  	 soPage.salesOrderApproval(test1);
////				 soPage.verifyProcessedScreen(test1);
////				 soPage.verifySOStatus("PENDING FULFILLMENT", test1);
////				 soPage.verifyCashSaleandPO("Purchase Order", test1); 
////				 String sales_order_url=driver.getCurrentUrl();
////				 soPage.navigatetoTransaction("Purchase Order");
////				 poPage.navigateToReceiveFromPO("Fulfilment");
////				 itemReceiptPage.saveItemReceiptfromPO(note, test1);
////				 driver.navigate().to(sales_order_url);
////				}
////			catch(Exception e)
////			{
////				test1.fail(" Trade Order via Contact Centre - Carpet/Vinyl Credit Cardis failed due to " +e.fillInStackTrace());
////			}
////			
////	}
////	}
//	
//	//testcase 30
////	@Test
////	public void NewSalesOrderViaContactCentre() throws InvalidFormatException, IOException
////	{
////		ExtentTest test1 = null;
////		test = extent.createTest("Verifying New Sales order via Contact Centre / Showroom - Carpet and existing UKFD Flooring");
////		ExcelReaderByMap reader2 = new ExcelReaderByMap();
////		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","TestCase30".trim());
////		for (Map<String,String> data: testData) {
////			String CustomerFirstname = data.get("Customer_Firstname");
////			String CustomerLastname = data.get("Customer_Lastname");
////			String itemname=data.get("Item_Name");
////			String quantity=data.get("Quantity");
////			String location=data.get("Location");
////			String shippingmethod=data.get("Shipping_Method");
////			String delivery_instructions=data.get("Delivery_Instructions");
////			String payment_method=data.get("Payment_Method");
////			String creditcardno=data.get("Credit_Card_Number");
////			String securitycode=data.get("Security_Code");
////			String nameoncard=data.get("NameOnCard");
////			String form=data.get("Sales_Order_Form");
////			String bin=data.get("Bin");
////			String terms=data.get("Terms");
////			String id=data.get("CustomerId");
////			String note=data.get("SupplierDeliveryNote");
////			String cardno=data.get("CreditCardNumber");
////			try
////			{
////				
////				String Customer_url="https://3460739-sb3.app.netsuite.com/app/common/entity/custjob.nl?id=";
////				customerPage=new CustomerPage();
////				soPage=new SalesOrderPage();
////				itemfulfilmentPage=new ItemFulfilment();
////				itemReceiptPage =new ItemReceiptPage();
////				poPage=new PurchaseOrderPage();
////				test1= test.createNode("Verifying sales order creation for item/items "+itemname+" using "+payment_method);
////				customerPage.navigateToCustomer("Sales Manager",Customer_url,id);
////				customerPage.clickNewSOFromCustomer();
////				soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),terms,cardno, test1);
////				soPage.verifyEmail("Thanks for your order!", test1);
////				soPage.salesOrderApproval(test1);
////				soPage.verifyProcessedScreen(test1);
////				soPage.verifySOStatus("PENDING FULFILLMENT", test1);
////				soPage.verifyEmail("Order Confirmation", test1); 
////				soPage.verifyCashSaleandPO("Purchase Order", test1); 
////				String sales_order_url=driver.getCurrentUrl();
////				soPage.navigatetoPO("Purchase Order");
////				poPage.navigateToReceiveFromPO("Fulfilment");
////				itemReceiptPage.saveItemReceiptfromPO(note, test1);
////				driver.navigate().to(sales_order_url);
////			}
////			catch(Exception e)
////			{
////				test.fail("New Sales order via Contact Centre / Showroom - Carpet and existing UKFD Flooring is failed due to "+e.fillInStackTrace());
////			}
////
////		}
////		
////	}
//	
//	//testcase 16
////	@Test(priority =1)
////	public void UKFDTradeOrderViaContactCentre() throws Exception
////	{
////		ExtentTest test1 = null;
////		test = extent.createTest("Verifying Trade Order via Contact Centre - Credit Card");
////		ExcelReaderByMap reader2 = new ExcelReaderByMap();
////		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx","TestCase16".trim());
////		for (Map<String,String> data: testData) {
////			String CustomerFirstname = data.get("Customer_Firstname");
////			String CustomerLastname = data.get("Customer_Lastname");
////			String email = data.get("Email");
////			String addr1 = data.get("Address1");
////			String addr2 = data.get("Address2");
////			String addr3 = data.get("Address3"); 
////			String city = data.get("City");
////			String phone=data.get("Phone");
////			String state = data.get("State");
////			String zip = data.get("Zip");
////			String leadsource=data.get("Lead_source");
////			String itemname=data.get("Item_Name");
////			String quantity=data.get("Quantity");
////			String location=data.get("Location");
////			String shippingmethod=data.get("Shipping_Method");
////			String delivery_instructions=data.get("Delivery_Instructions");
////			String payment_method=data.get("Payment_Method");
////			String creditcardno=data.get("Credit_Card_Number");
////			String expirydata=data.get("Expiry_Date");
////			String securitycode=data.get("Security_Code");
////			String nameoncard=data.get("NameOnCard");
////			String form=data.get("Sales_Order_Form");
////			String customertype=data.get("Customer_type");
////			String bin=data.get("Bin");
////			String Terms="";
////			try {
////			test1= test.createNode("Verifying sales order creation for item "+itemname+" using "+payment_method);
////			 customerPage = new CustomerPage();
////			 soPage=new SalesOrderPage();
////			 opprPage=new OpportunityPage();
////		 	 quotePage=new QuotePage();
////			 customerPage.enter_Customer_details(CustomerFirstname, CustomerLastname, email, phone, addr1, addr2, addr3, city, state, zip, customertype,"Trade Sales",creditcardno,nameoncard,expirydata,securitycode,payment_method, test1);
////			 opprPage.enterOpprntyDetails(leadsource,test1);
////			 quotePage.enterQuoteDetails(location,itemname,quantity,shippingmethod,test1);
////			 soPage.enterSoDetails(form, delivery_instructions, shippingmethod, CustomerFirstname, itemname, quantity, payment_method.trim(),Terms, test1);
////			 soPage.verifyEmail("Thanks for your order!", test1);
////			 soPage.salesOrderApproval(test1);
////			 soPage.verifySOStatus("PENDING FULFILLMENT",test1);
////			 soPage.verifyEmail("Your order has been confirmed", test1);
////			 soPage.verifyCashSaleandPO("Cash Sale", test1);
////			} 
////			
////		   catch (Exception e) 
////			{
////				test.fail(" Trade Order via Contact Centre - Credit Card is failed due to "+e.fillInStackTrace());
////			}
////		}
////	
////
////	
////	

	Date currentTime = Calendar.getInstance().getTime();
	
}
	
	



