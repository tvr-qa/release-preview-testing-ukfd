package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.util.TestUtil;

public class CashSale extends TestUtil{
	
	@FindBy(xpath = "//input[@name='return']")
	WebElement returnButton;
	
	public CashSale()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public void navigateToReturnFromCashSale()
	{
		
		eleAvailability(driver, returnButton, 20);
		returnButton.click();
	}

}
