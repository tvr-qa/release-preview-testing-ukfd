package com.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.qa.util.TestUtil;

public class TransferOrderToCreateSearch extends TestUtil {
	
	@FindBy(xpath = "//input[@id='savesearch']")
	WebElement Edit_search;
	
	@FindBy(xpath = "//a[@id='results_tabtxt']")
	WebElement results_tab;
	
	@FindBy(xpath = "//input[@name='inpt_rffield']")
	WebElement input_dropdown;
	
	@FindBy(xpath = "//div[@class='dropdownDiv']//div")
	List<WebElement> dropdown_div;
	
	@FindBy(xpath = "//input[@id='returnfields_addedit']")
	WebElement add_button;
	
	@FindBy(xpath = "//input[@id='secondarysubmitter']")
	WebElement preview_button;
	
	@FindBy(xpath = "//table[@id='div__bodytab']//tr[contains(@id,'row')]//td[count(//table[@id='div__labtab']//tr//td[@data-label='Document Number']//preceding-sibling::td)+1]//a")
	List<WebElement> Transaction_nrs;
		
	
	public TransferOrderToCreateSearch()
	{
		PageFactory.initElements(driver, this);
		
	}
	
	public void navigate_to_search(String tran_No,ExtentTest test) throws InterruptedException
	{
		
		
		driver.navigate().to("https://3460739-sb2.app.netsuite.com/app/common/search/searchresults.nl?searchid=1893293&whence=");
		eleAvailability(driver, Edit_search, 10);
		Edit_search.click();
		Thread.sleep(2000);
		results_tab.click();
		Thread.sleep(1000);
		input_dropdown.click();
		for(int i=0;i<dropdown_div.size();i++)
		{
			if(dropdown_div.get(i).getText().trim().equals("Document Number"))
			{
				dropdown_div.get(i).click();
				break;
			}
		}
		add_button.click();
		preview_button.click();
		boolean flag=false;
		for(int i=0;i<Transaction_nrs.size();i++)
		{
			if(Transaction_nrs.get(i).getText().equals(tran_No))
			{
				flag=true;
				test.pass("Transfer orders to create search is displaying the sales order for which transfer order should be created");
				break;
			}
			
		}
		if(flag==false)
		{
			test.fail("Transfer orders to create search is not displaying the sales order for which transfer order should be created");
		}
		
		
		
		
	}
	

}
