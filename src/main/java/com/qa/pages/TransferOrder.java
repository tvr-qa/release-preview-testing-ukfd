package com.qa.pages;

import java.util.List;

import javax.swing.text.Document;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.qa.util.TestUtil;

public class TransferOrder extends TestUtil{
	
	
	@FindBy(xpath = "//li[@data-title='Transactions']")
	WebElement transcations_Link;
	
	@FindBy(linkText="Inventory")
	WebElement inventory_Link;
	
	@FindBy(linkText="Enter Transfer Orders")
	WebElement transfer_OrderLink;
	
	@FindBy(xpath = "//a[@class='ns-scroll-button ns-scroll-button-bottom']")
	WebElement arrow;
	
	@FindBy(xpath = "//input[@name='inpt_subsidiary']")
	WebElement subsidiary_dropdown;
	
	@FindBy(xpath = "//div[@class='dropdownDiv']//div")
	List<WebElement> dropdown_div;
	
	@FindBy(xpath = "//input[@name='inpt_location']")
	WebElement from_dropdown;
	
	@FindBy(xpath = "//input[@name='inpt_transferlocation']")
	WebElement to_dropdown;
	
	@FindBy(xpath = "//span[@id='parent_actionbuttons_item_item_fs']")
	WebElement item_arrow;
	
	@FindBy(xpath="//a[@id='item_popup_list']")
	WebElement item_list_option;
	
	@FindBy(xpath="//table//input[@id='st' and  @class='inputgray' and @type='text']")
	WebElement Item_searchbox_textbox;
	
	@FindBy(xpath="//input[@id='Search']")
	WebElement Item_search_searchbox_button;
	
	@FindBy(xpath = "//div[@id='popup_outerdiv']//div[@id='inner_popup_div']//table//tr//td//following-sibling::td//a")
	List<WebElement> searchList;
	
	@FindBy(xpath="//div[@id='inner_popup_div']//table/tbody/tr//following-sibling::tr//td//following-sibling::td//a")
	List<WebElement> items_list;
	
	@FindBy(xpath = "//table[@id='inventory_splits']//tr//following-sibling::tr[contains(@class,'uir-machine-row')]//td[5]")
	List<WebElement> quantity_to_transfer_div;
	
	@FindBy(xpath = "//input[@id='adjustqtyby_formattedValue']")
	WebElement quantity_textbox;
	
	@FindBy(xpath = "//a[@id='inventorydetail_helper_popup']")
	WebElement inventory_detail_popup;
	
	@FindBy(xpath = "//iframe[@id='childdrecord_frame']")
	WebElement frame;
	
	@FindBy(xpath = "//input[@name='inpt_binnumber']")
	WebElement select_bin_input;
	
	@FindBy(xpath = "//table[@id='item_splits']//tr[contains(@class,'uir-machine-row-focused')]//td[count(//tr[@id='item_headerrow']//div[text()='Quantity']//parent::td//preceding-sibling::td)+1]")
	WebElement quantity_div;
	
	@FindBy(xpath = "//input[@id='quantity_formattedValue']")
	WebElement quantity_input;
	
	@FindBy(xpath = "//input[@id='item_addedit']")
	WebElement add_item;
	
	@FindBy(xpath = "//input[@id='btn_secondarymultibutton_submitter']")
	WebElement save_transfer_order;
	
	@FindBy(xpath = "//div[@class='uir-record-status']")
	WebElement transferOrderStatus;
	
	@FindBy(xpath = "//div[@class='uir-record-id']")
	WebElement transferOrderId;
	
	@FindBy(xpath = "//table[@id='item_splits']//tr//following-sibling::tr//td[count(//td[@data-label='Committed']//preceding-sibling::td)+1]")
	List<WebElement> committed_quantity;
	
	
	
	Actions action=new Actions(driver);
	JavascriptExecutor js=(JavascriptExecutor)driver;
	public TransferOrder()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public void navigate_to_transfer_order() throws InterruptedException
	{
		Thread.sleep(1000);
		action.moveToElement(transcations_Link).build().perform();
		Thread.sleep(1000);
		action.moveToElement(inventory_Link).build().perform();
		Thread.sleep(1000);
		action.moveToElement(arrow).build().perform();
		Thread.sleep(2000);
		transfer_OrderLink.click();
		if(isAlertPresent_()==true)
		{
			driver.switchTo().alert().accept();
		}		
	}
	public String create_Transfer_Order(String itemname,String quantity,ExtentTest test) throws InterruptedException
	{
		
		navigate_to_transfer_order();
		eleAvailability(driver, subsidiary_dropdown, 10);
		selectDropdownValue(subsidiary_dropdown, dropdown_div, "UKFD");
		Thread.sleep(3000);
		selectDropdownValue(to_dropdown, dropdown_div, "Prologis Park : PP - Warehouse");
		Thread.sleep(1000);
		selectDropdownValue(from_dropdown, dropdown_div, "Logix Road : LR - Warehouse".trim());
		selectValueFromList(item_arrow, item_list_option, Item_searchbox_textbox, Item_search_searchbox_button, searchList, itemname.trim());
		quantity_div.click();
		eleClickable(driver, quantity_input, 5);
		quantity_input.sendKeys(quantity.trim());
		add_item.click();
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(1000);
		save_transfer_order.click();
		eleAvailability(driver, transferOrderStatus, 10);
		String transferStatus=transferOrderStatus.getText().trim();
		String transferId=transferOrderId.getText().trim();
		if(transferStatus.equals("Pending Fulfillment"))
		{
			test.pass("Transfer order status is displaying is: "+transferStatus +" and created with id: "+transferId);
		}
		else
		{
			test.fail("Transfer order status is not correctly displayed");
		}
		WebElement committed_quantity_div=driver.findElement(By.xpath("//a[text()='"+itemname.trim()+"']//following::td[count(//td[@data-label='Committed']//preceding-sibling::td)]"));
		waitUntilScriptIsScheduled(committed_quantity_div,"0",2000,itemname);
		if(committed_quantity.get(0).getText().trim().equals(quantity.trim()))
		{
			test.pass("Stock is auto committed");
		}
		else
		{
			test.fail("Stock is not auto committed");
		}
		return transferId;
		
		
		
	}
	
	

}
