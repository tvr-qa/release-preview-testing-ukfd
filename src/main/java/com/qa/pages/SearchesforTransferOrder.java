package com.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.qa.util.TestUtil;

public class SearchesforTransferOrder extends TestUtil {
	
	
   @FindBy(xpath = "//table[@id='div__bodytab']//tr[contains(@id,'row')]//td[count(//table[@id='div__labtab']//tr//td[@data-label='Document Number']//preceding-sibling::td)+1]")
   List<WebElement> doc_nrs;



           public SearchesforTransferOrder()
           {
	          PageFactory.initElements(driver, this);
           }
           
           public boolean verifyDocnrs(String docnr,List<WebElement> list,ExtentTest test)
           {
        	   boolean flag=false;
        	   for(int i=0;i<list.size();i++)
        	   {
        		   if(list.get(i).getText().trim().equals(docnr.trim()))
        		   {
        			  flag=true;
        		   }
        	   }
			return flag;
        	  
           }
           
           public void verifyTOtoShipFromHincleySearch(String docnr,ExtentTest test)
           {
        	  driver.navigate().to("https://3460739-sb2.app.netsuite.com/app/common/search/searchresults.nl?searchid=1893292&whence=");
        	  boolean flag= verifyDocnrs(docnr, doc_nrs, test);
        	  if(flag==true)
        	  {
        		  test.pass("Transfer order is displaying in the search before creating item fulfillment");
        	  }
        	  else
        	  {
        		  test.fail("Transfer is not displaying in the search before creating item fulfillment");
        	  }
        	   
           }
           
           public void verifyAfterTOtoShipFromHincleySearch(String docnr,ExtentTest test)
           {
         	  driver.navigate().to("https://3460739-sb2.app.netsuite.com/app/common/search/searchresults.nl?searchid=1893292&whence=");
        	  boolean flag= verifyDocnrs(docnr, doc_nrs, test);
        	  if(flag==false)
        	  {
        		  test.pass("Transfer order is not displaying in the search after creating item fulfillment");
        	  }
        	  else
        	  {
        		  test.fail("Transfer is displaying in the search after creating item fulfillment");
        	  }
        	   
           }
           public void verifyTOPendingReceiptSearch(String docnr,ExtentTest test)
           {
        	  driver.navigate().to("https://3460739-sb2.app.netsuite.com/app/common/search/searchresults.nl?searchid=1893291&whence=");
        	  boolean flag= verifyDocnrs(docnr, doc_nrs, test);
        	  if(flag==true)
        	  {
        		  test.pass("Transfer order is displaying in the search before creating item receipt");
        	  }
        	  else
        	  {
        		  test.fail("Transfer is not displaying in the search before creating item receipt");
        	  }
        	   
           }
           
           public void verifyAfterPendingReceiptSearch(String docnr,ExtentTest test)
           {
         	  driver.navigate().to("https://3460739-sb2.app.netsuite.com/app/common/search/searchresults.nl?searchid=1893291&whence=");
        	  boolean flag= verifyDocnrs(docnr, doc_nrs, test);
        	  if(flag==false)
        	  {
        		  test.pass("Transfer order is not displaying in the search after creating item receipt");
        	  }
        	  else
        	  {
        		  test.fail("Transfer is displaying in the search after creating item receipt");
        	  }
        	   
           }
           

	
	
	
}
